<?php

/**
 * @file
 * Forms to configure ekomi for Drupal.
 */

function uc_ekomi_settings() {
  $form['uc_ekomi_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate eKomi.'),
    '#default_value' => uc_ekomi_var('active'),
  );
  $form['uc_ekomi_interface_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Your eKomi ID'),
    '#size' => 10,
    '#maxlength' => 5,
    '#default_value' => uc_ekomi_var('interface_id'),
  );
  $form['uc_ekomi_interface_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Your eKomi password'),
    '#size' => 10,
    '#maxlength' => 30,
    '#default_value' => uc_ekomi_var('interface_password'),
  );
  $dir_feedback = uc_ekomi_path_feedback();
  $dir_feedback_ok = file_check_directory($dir_feedback, TRUE);
  $dir_widget = uc_ekomi_path_widget();
  $dir_widget_ok = file_check_directory($dir_widget, TRUE);
  $form['uc_ekomi_check'] = array(
    '#type' => 'markup',
    '#prefix' => '<div>',
    '#suffix' => '</div>',
    '#value' => t('<h4>Environment check:</h4>
           <p>Feedback: '. $dir_feedback .' '. _uc_ekomi_check_symbol($dir_feedback_ok) .'</p>
           <p>Widget: '. $dir_widget .' '. _uc_ekomi_check_symbol($dir_widget_ok) .'</p>
           <p>allow_url_fopen: '. _uc_ekomi_check_symbol(ini_get("allow_url_fopen")) .'</p>
           <p>fsockopen: '. _uc_ekomi_check_symbol(function_exists("fsockopen")) .'</p>'),
  );

  return system_settings_form($form);
}

function _uc_ekomi_check_symbol($status) {
  if ($status) {
    return 'OK';
  }
  else {
    return 'ERROR';
  }
}
