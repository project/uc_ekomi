<?php

/**
 * @file
 * This file contains all routines to access the database and returns the
 * relevant results or result sets.
 */

/**
 * Return the date of the last completed order.
 */
function uc_ekomi_lastdate($order_status = '') {
  if (empty($order_status)) {
    $order_status = uc_ekomi_var('order_status_trigger');
  }
  $sql = "SELECT modified FROM {uc_orders}
    WHERE order_status = '%s'
    ORDER BY modified DESC
    LIMIT 1";
  return db_result(db_query($sql, $order_status));
}

/**
 * Find out if a customer has already been included into the ekomi
 * database table, either for current or past customers.
 */
function uc_ekomi_customer_by_email($email, $current = TRUE) {
  $sql = "SELECT email FROM ";
  if ($current) {
    $sql .= "{uc_ekomi_customer}";
  }
  else {
    $sql .= "{uc_ekomi_customer_old}";
  }
  $sql .= " WHERE email = '%s'";
  return db_result(db_query($sql, $email));
}

/**
 * Find and return all completed orders since a given date and time.
 */
function uc_ekomi_orders($last_date, $order_status = '') {
  $data  = array();
  $where = '';
  $i     = 0;
  if (empty($order_status)) {
    $order_status = uc_ekomi_var('order_status_trigger');
  }
  if (strpos($last_date, '-') == TRUE OR strpos($last_date, ':') == TRUE) {
    $sql = "SELECT DATE_SUB('%s', INTERVAL 30 DAY) AS date FROM {uc_orders}";
    $fetch = db_result(db_query($sql, $last_date));
    $date  = $fetch['date'];
  }
  else {
    $date = $last_date - 2592000;
  }
  $sql = "SELECT * FROM {uc_orders} WHERE modified >= '%s' AND order_status='%s'";
  $result = db_query($sql, $date, $order_status);
  while ($fetch = db_fetch_object($result)) {
    $data[$i]['cid'] = $fetch->uid;
    $i++;
  }
  return $data;
}

/**
 * Find an retrieve customer detail for a given customer id.
 */
function uc_ekomi_customer_data($cid) {
  $data = array();
  $sql = "SELECT * FROM {uc_orders} WHERE uid = '%s' LIMIT 1";
  $fetch = db_fetch_object(db_query($sql, $cid));
  $data['firstname'] = $fetch->billing_first_name;
  $data['lastname']  = $fetch->billing_last_name;
  $data['email']     = $fetch->primary_email;
  return $data;
}

/**
 * Find and retrieve all customers (only $max in one run through) that need
 * to sent an email to ask for their feedback.
 */
function uc_ekomi_customers($start_id, $max) {
  if ($start_id == 0) {
    $sql = "SELECT * FROM {uc_ekomi_customer} ORDER BY eid";
    $result = db_query_range($sql, 0, $max);
  }
  else {
    $sql = "SELECT * FROM {uc_ekomi_customer} WHERE
            eid >= %s AND eid <= (%s + %s)
            ORDER BY eid";
    $result = db_query($sql, $start_id, $start_id, $max);
  }
  $customers = array();
  while ($fetch = db_fetch_object($result)) {
    $customers[] = $fetch;
  }
  return $customers;
}

/**
 * Save customer data into the current customer database table.
 */
function uc_ekomi_save_customer($cid, $email, $firstname, $lastname) {
  $sql = "INSERT INTO {uc_ekomi_customer}
    (cid, email, firstname, lastname, etime)
    VALUES
    ('%s', '%s', '%s', '%s', '%s')";
  db_query($sql, $cid, $email, $firstname, $lastname, time());
}

/**
 * Move customer detail from the current to the past customer database table.
 */
function uc_ekomi_move_customer($cid, $email) {
  $sql = "DELETE FROM {uc_ekomi_customer} WHERE cid = '%s'";
  db_query($sql, $cid);
  $sql = "INSERT INTO {uc_ekomi_customer_old} (cid, email) VALUES ('%s','%s')";
  db_query($sql, $cid, $email);
}
