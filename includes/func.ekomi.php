<?php

/**
 * @file
 * Ekomi specific functions.
 */

/**
 * Return the path for html files with individual feedback content.
 */
function uc_ekomi_path_feedback() {
  return _uc_ekomi_path('feedback');
}

/**
 * Return the path for images with feedback and the ekomi logo.
 */
function uc_ekomi_path_widget() {
  return _uc_ekomi_path('widget');
}

/**
 * Internal function to determine and if required create the directories.
 */
function _uc_ekomi_path($sub = NULL) {
  if (isset($sub)) {
    $path = _uc_ekomi_path() .'/'. $sub;
  }
  else {
    $path = file_directory_path() .'/ekomi';
  }
  if (!file_exists($path)) {
    mkdir($path);
  }
  return $path;
}

/**
 * Implementation of hook_mail().
 */
function uc_ekomi_mail($key, &$message, $params) {
  switch ($key) {
    case 'askforfeedback':
      $message['subject'] = $params['subject'];
      $message['body'] = $params['plain'];
      break;
  }
}

/**
 * Send all active customer ids and their email addresses to ekomi to validate
 * feedback coming from authorized users.
 */
function uc_ekomi_generate() {
  $txt = '';
  $ids = '';
  $sql = "SELECT email,cid FROM {uc_ekomi_customer}";
  $customers = db_query($sql);
  while ($customer = db_fetch_object($customers)) {
    $ids .= $customer->cid .'|'; 
    $txt .= $customer->email; 
  }
  $records = 'shop_id='. uc_ekomi_var('interface_id') .'&string='. md5($txt.$count) .'&customers='. $ids .'&check='. uc_ekomi_var('interface_password');
  $data = uc_ekomi_send_post('api.ekomi.de', '/hash.php', $records);
}
