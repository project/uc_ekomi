<?php

/**
 * @file
 * Functions to post data back to ekomi.
 */

/**
 * Encode data and send it to the ekomi server, return the feedback.
 */
function uc_ekomi_send_post($host, $path, $data) {
  $result = '';
  $fp = fsockopen($host, 80);
  fputs($fp, "POST $path HTTP/1.1\r\n");
  fputs($fp, "Host: $host\r\n");
  fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
  fputs($fp, "Content-length: ". strlen($data) ."\r\n");
  fputs($fp, "Connection: close\r\n\r\n");
  fputs($fp, $data);
  while (!feof($fp)) {
    $result .= fgets($fp, 128);
  }
  fclose($fp);
  $data = explode("\r\n\r\n", $result);
  $data = $data[1]; 
  return _uc_ekomi_decode_chunked($data);
}

/**
 * Decode data received from the ekomi server.
 */
function _uc_ekomi_decode_chunked($string) {
  $lines   = explode("\r\n", $string);
  $i       = 0;
  $length  = 999;
  $content = '';
  foreach ($lines as $line) {
    $i++;
    if ($i%2 == 1) {
      $length = hexdec($line);
    }
    elseif ($length == strlen($line)) {
      $content .= $line;
    }
    if ($length == 0) {
      break;
    }
  }
  return $content;
}
