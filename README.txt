
-- SUMMARY --

This module allows you to easily and quickly integrate the eKomi webservice
into your website with an Ubercart online shop.

More details about eKomi can be found on eKomi's website (http://www.ekomi.de).

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.

* Cron to run at least once a day.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  uc_ekomi module:

  - administer ekomi

* Configure eKomi in Administer >> Store Administration >> eKomi >> Settings

* Make the eKomi block visible in a region of your site in Structure >> blocks

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* J�rgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de

